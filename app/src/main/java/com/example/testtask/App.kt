package com.example.testtask

import android.app.Application
import com.example.testtask.di.AppComponent
import com.example.testtask.di.AppModule
import com.example.testtask.di.DaggerAppComponent

class App : Application() {

    private var appComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    fun getAppComponent(): AppComponent? {
        return appComponent
    }
}