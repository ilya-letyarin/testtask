package com.example.testtask.data

import com.example.testtask.domain.entity.CountryEntity

interface Repository {
    val countriesCache : MutableList<CountryEntity>
    suspend fun getAllInfo(isNeedUpdate: Boolean): List<CountryEntity>
}