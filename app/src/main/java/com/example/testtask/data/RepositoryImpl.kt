package com.example.testtask.data

import com.example.testtask.data.mapper.MapperData
import com.example.testtask.data.network.NetworkClient
import com.example.testtask.domain.entity.CountryEntity
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
    private val client: NetworkClient,
    private val mapper: MapperData,
) : Repository {
    override val countriesCache = mutableListOf<CountryEntity>()

    override suspend fun getAllInfo(isNeedUpdate: Boolean): List<CountryEntity> {
        if (countriesCache.isEmpty() || isNeedUpdate) {
            val countries = client.getAllInfo()
            val mapped = countries.map {
                mapper.mapToEntity(it)

            }
            countriesCache.addAll(mapped)

            return countriesCache
        }
        return countriesCache
    }
}