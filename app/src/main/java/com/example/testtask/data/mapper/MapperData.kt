package com.example.testtask.data.mapper

import com.example.testtask.data.responce.CountryResponse
import com.example.testtask.data.responce.CurrencyResponse
import com.example.testtask.domain.entity.CountryEntity
import com.example.testtask.domain.entity.CurrencyEntity

class MapperData {
    fun mapToEntity(country: CountryResponse): CountryEntity =
        CountryEntity(
            name = country.name,
            image = country.flags.png,
            capital = country.capital ?: "",
            currencies = country.currencies?.map { currency ->
                mapToCurrencyEntity(currency)
            } ?: listOf(),
            region = country.region,
            timezones = country.timezones
        )

    private fun mapToCurrencyEntity(currency: CurrencyResponse): CurrencyEntity =
        CurrencyEntity(
            code = currency.code,
            name = currency.name,
            symbol = currency.symbol
        )
}