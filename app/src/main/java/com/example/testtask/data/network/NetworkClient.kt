package com.example.testtask.data.network

import com.example.testtask.data.responce.CountryResponse
import retrofit2.http.GET

interface NetworkClient {
    @GET("all")
    suspend fun getAllInfo(): List<CountryResponse>

}