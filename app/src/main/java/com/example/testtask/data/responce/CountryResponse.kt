package com.example.testtask.data.responce

data class CountryResponse(
    val alpha2Code: String,
    val alpha3Code: String,
    val altSpellings: List<String>?,
    val area: Double,
    val borders: List<String>?,
    val callingCodes: List<String>,
    val capital: String?,
    val cioc: String?,
    val currencies: List<CurrencyResponse>?,
    val demonym: String,
    val flag: String,
    val flags: FlagsResponse,
    val independent: Boolean,
    val languages: List<LanguageResponse>,
    val latlng: List<Double>,
    val name: String,
    val nativeName: String,
    val numericCode: String,
    val population: Int,
    val region: String,
    val regionalBlocs: List<RegionalBlocResponse>?,
    val subregion: String,
    val timezones: List<String>,
    val topLevelDomain: List<String>,
    val translations: TranslationsResponse
)