package com.example.testtask.data.responce

data class CurrencyResponse(
    val code: String,
    val name: String,
    val symbol: String
)