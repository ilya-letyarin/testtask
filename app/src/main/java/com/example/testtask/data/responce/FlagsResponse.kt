package com.example.testtask.data.responce

data class FlagsResponse(
    val png: String,
    val svg: String
)