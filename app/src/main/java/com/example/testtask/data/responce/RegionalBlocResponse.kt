package com.example.testtask.data.responce

data class RegionalBlocResponse(
    val acronym: String,
    val name: String
)