package com.example.testtask.data.responce

data class TranslationsResponse(
    val br: String,
    val de: String,
    val es: String,
    val fa: String,
    val fr: String,
    val hr: String,
    val hu: String,
    val `it`: String,
    val ja: String,
    val nl: String,
    val pt: String
)