package com.example.testtask.di

import com.example.testtask.presentation.activity.MainActivity
import com.example.testtask.presentation.fragments.CountryFragment
import com.example.testtask.presentation.fragments.DetailsFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class ,ViewModelModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)
    fun inject(fragment: CountryFragment)
    fun inject(fragment: DetailsFragment)
}