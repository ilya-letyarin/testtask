package com.example.testtask.di

import android.app.Application
import android.content.Context
import com.example.testtask.data.Repository
import com.example.testtask.data.RepositoryImpl
import com.example.testtask.data.mapper.MapperData
import com.example.testtask.data.network.NetworkClient
import com.example.testtask.data.network.Retrofit
import com.example.testtask.domain.CountryInteractor
import com.example.testtask.domain.CountryInteractorImpl
import com.example.testtask.presentation.adapters.CountryAdapter
import com.example.testtask.presentation.mapper.Mapper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    fun providesApplicationContext(): Context = application.applicationContext

    @Provides
    @Singleton
    fun provideNetwork(): NetworkClient = Retrofit.instance.create(NetworkClient::class.java)

    @Provides
    @Singleton
    fun provideMapperData() = MapperData()

    @Provides
    @Singleton
    fun provideRepository(): Repository = RepositoryImpl(provideNetwork(), provideMapperData())

    @Provides
    @Singleton
    fun provideInteractor(): CountryInteractor = CountryInteractorImpl(provideRepository())

    @Provides
    @Singleton
    fun provideMapper(): Mapper = Mapper()

    @Singleton
    @Provides
    fun provideCountryAdapter(): CountryAdapter = CountryAdapter(providesApplicationContext())
}