package com.example.testtask.domain

import com.example.testtask.domain.entity.CountryEntity

interface CountryInteractor {
    suspend fun getAllInfo(isNeedUpdate: Boolean) : List<CountryEntity>

    suspend fun getCountryDescription(name: String) : CountryEntity
}