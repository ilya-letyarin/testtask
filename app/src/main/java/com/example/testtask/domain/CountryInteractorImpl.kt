package com.example.testtask.domain

import com.example.testtask.data.Repository
import com.example.testtask.domain.entity.CountryEntity
import javax.inject.Inject

class CountryInteractorImpl @Inject constructor(
    private val repository: Repository,
) : CountryInteractor {

    override suspend fun getAllInfo(isNeedUpdate: Boolean): List<CountryEntity> =
        repository.getAllInfo(isNeedUpdate)

    override suspend fun getCountryDescription(name: String): CountryEntity {
        val item = repository.countriesCache
            .filter {
                it.name.contains(name)
            }
            .get(0)
        return item
    }
}


