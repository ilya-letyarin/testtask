package com.example.testtask.domain.entity

data class CountryEntity(
    val name: String,
    val image: String,
    val region : String,
    val capital: String,
    val currencies: List<CurrencyEntity>,
    val timezones: List<String>
)