package com.example.testtask.domain.entity

data class CurrencyEntity(
    val code: String,
    val name: String,
    val symbol: String
)