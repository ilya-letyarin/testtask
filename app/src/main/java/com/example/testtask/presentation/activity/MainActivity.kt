package com.example.testtask.presentation.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.testtask.App
import com.example.testtask.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as App).getAppComponent()?.inject(this)
    }
}