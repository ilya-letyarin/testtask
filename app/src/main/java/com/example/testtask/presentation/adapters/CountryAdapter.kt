package com.example.testtask.presentation.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.testtask.R
import com.example.testtask.databinding.ItemCountryBinding
import com.example.testtask.presentation.model.CountryItemUiModel
import javax.inject.Inject

class CountryAdapter @Inject constructor(private val context: Context) :
    RecyclerView.Adapter<CountryAdapter.CountryViewHolder>() {
    private var countryList: ArrayList<CountryItemUiModel> = arrayListOf()

    @SuppressLint("NotifyDataSetChanged")
    fun setListCountry(list: List<CountryItemUiModel>) {
        countryList.clear()
        countryList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CountryViewHolder(ItemCountryBinding.inflate(inflater, parent, false), context)
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        val album = countryList[position]
        holder.bind(album)
        holder.itemView.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("name",album.name)
            val navigation = findNavController(it)
            navigation.navigate(R.id.action_countryFragment_to_detailsFragment,bundle)

        }
    }

    override fun getItemCount(): Int {
        return countryList.size
    }

    class CountryViewHolder(private val binding: ItemCountryBinding, private val context: Context) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(country: CountryItemUiModel) {
            Glide.with(context).load(country.image).into(binding.flagIV)
            binding.itemNameCountryYv.text = country.name

        }
    }
}