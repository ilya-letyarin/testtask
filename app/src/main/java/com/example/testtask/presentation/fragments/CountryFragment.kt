package com.example.testtask.presentation.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.example.testtask.App
import com.example.testtask.R
import com.example.testtask.databinding.FragmentCountryBinding
import com.example.testtask.presentation.adapters.CountryAdapter
import com.example.testtask.presentation.viewModel.CountryViewModel
import com.example.testtask.presentation.viewModel.ViewModelFactory
import kotlinx.coroutines.launch
import javax.inject.Inject

class CountryFragment : Fragment() {

    lateinit var binding : FragmentCountryBinding
    @Inject
    lateinit var adapter : CountryAdapter

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    lateinit var viewModel : CountryViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.application as App).getAppComponent()?.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.fragment_country, container, false)
        binding = FragmentCountryBinding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()

        binding.updateBtn.setOnClickListener {
            viewModel.getCountries()
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted{
            viewModel.countries.collect{
                adapter.setListCountry(it)
            }
        }
        viewLifecycleOwner.lifecycleScope.launchWhenCreated{
            viewModel.countriesError.collect{
                if (it != null) {
                    Toast.makeText(context,it,Toast.LENGTH_LONG).show()
                }
            }
        }
    }
    private fun init(){
        viewModel = ViewModelProvider(this, viewModelFactory)[CountryViewModel::class.java]
        val recyclerView: RecyclerView = binding.countryRV
        recyclerView.adapter = adapter
        viewModel.getCountries()
    }
}