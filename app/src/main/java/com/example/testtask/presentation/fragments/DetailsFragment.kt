package com.example.testtask.presentation.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.example.testtask.App
import com.example.testtask.R
import com.example.testtask.databinding.FragmentDetalsBinding
import com.example.testtask.presentation.model.CountryDescriptionUiModel
import com.example.testtask.presentation.viewModel.CountryDetailsViewModel
import com.example.testtask.presentation.viewModel.ViewModelFactory
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailsFragment : Fragment() {

    private lateinit var binding: FragmentDetalsBinding

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    lateinit var viewModel: CountryDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.application as App).getAppComponent()?.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.fragment_detals, container, false)
        binding = FragmentDetalsBinding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.countriesDescription
                .collect {
                if (it != null) {
                    show(it)
                }
            }
        }
    }

    private fun show(details: CountryDescriptionUiModel) {
        binding.capitalTV.text = details.capital
        binding.currencyTV.text = details.currencies.toString()
        binding.timezoneTv.text = details.timezones.toString()
        binding.regionTV.text = details.region
        binding.nameCountryTV.text = details.name
        Glide.with(this).load(details.image).into(binding.detailsFlagIV)

    }

    private fun init() {
        viewModel = ViewModelProvider(this, viewModelFactory)[CountryDetailsViewModel::class.java]
        arguments?.getString("name")?.let { viewModel.getCountriesDescription(it) }
    }
}