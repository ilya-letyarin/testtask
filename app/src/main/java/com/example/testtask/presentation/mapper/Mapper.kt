package com.example.testtask.presentation.mapper

import com.example.testtask.domain.entity.CountryEntity
import com.example.testtask.domain.entity.CurrencyEntity
import com.example.testtask.presentation.model.CountryDescriptionUiModel
import com.example.testtask.presentation.model.CountryItemUiModel
import com.example.testtask.presentation.model.CurrencyUiModel

class Mapper {
    fun mapToUiModel(country: CountryEntity): CountryItemUiModel =
        CountryItemUiModel(
            name = country.name,
            image = country.image
        )

    fun mapToDescriptionUiModel(country: CountryEntity): CountryDescriptionUiModel =
        CountryDescriptionUiModel(
            region = country.region,
            capital = country.capital,
            currencies = country.currencies.map {
                mapCurrenciesToUiModel(it)
            },
            timezones = country.timezones,
            image = country.image,
            name = country.name
        )

    private fun mapCurrenciesToUiModel(currency: CurrencyEntity): CurrencyUiModel =
        CurrencyUiModel(
            code = currency.code,
            name = currency.name,
            symbol = currency.symbol
        )
}