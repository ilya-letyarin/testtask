package com.example.testtask.presentation.model

data class CountryDescriptionUiModel (
    val image : String,
    val name : String,
    val region : String,
    val capital: String,
    val currencies: List<CurrencyUiModel>,
    val timezones: List<String>
        )