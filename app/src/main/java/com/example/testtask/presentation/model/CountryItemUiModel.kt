package com.example.testtask.presentation.model

data class CountryItemUiModel(
    val name: String,
    val image: String,
)