package com.example.testtask.presentation.model

data class CurrencyUiModel(
    val code: String,
    val name: String,
    val symbol: String
)