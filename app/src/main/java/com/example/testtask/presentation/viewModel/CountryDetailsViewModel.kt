package com.example.testtask.presentation.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testtask.domain.CountryInteractor
import com.example.testtask.presentation.mapper.Mapper
import com.example.testtask.presentation.model.CountryDescriptionUiModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

class CountryDetailsViewModel @Inject constructor(
    private val interactor: CountryInteractor,
    private val mapper: Mapper,
) : ViewModel() {

    private val _countriesDescription = MutableStateFlow<CountryDescriptionUiModel?>(null)
    val countriesDescription: StateFlow<CountryDescriptionUiModel?> =
        _countriesDescription.asStateFlow()

    fun getCountriesDescription(name: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val models = interactor.getCountryDescription(name)
            _countriesDescription.emit(mapper.mapToDescriptionUiModel(models))
        }
    }
}