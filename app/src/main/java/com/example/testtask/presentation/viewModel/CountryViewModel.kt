package com.example.testtask.presentation.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testtask.domain.CountryInteractor
import com.example.testtask.presentation.mapper.Mapper
import com.example.testtask.presentation.model.CountryItemUiModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class CountryViewModel @Inject constructor(
    private val interactor: CountryInteractor,
    private val mapper: Mapper,
) : ViewModel() {

    private val _countries = MutableStateFlow(listOf<CountryItemUiModel>())
    val countries: StateFlow<List<CountryItemUiModel>> = _countries.asStateFlow()

    private val _countriesError = MutableSharedFlow<String?>()
    val countriesError: SharedFlow<String?> =
        _countriesError.asSharedFlow()

    fun getCountries() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _countries.value = interactor.getAllInfo(false)
                    .map {
                        mapper.mapToUiModel(it)
                    }

            }catch (e : Exception) {
                _countriesError.emit(e.message)

            }
        }
    }
}


